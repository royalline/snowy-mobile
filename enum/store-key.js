export const STORE_KEY_ENUM = {
	envKey: "envKey",
	allEnv: "allEnv",
	tenantDomain: 'tenantDomain',
	sysBaseConfig: 'sysBaseConfig',
	homeConfigs: "homeConfigs",
	userInfo: 'userInfo',
	userMenus: 'userMenus',
	userMobileMenus: 'userMobileMenus',
	dictTypeTreeData: 'dictTypeTreeData',
	refreshKey: 'refreshKey',
	refreshFlag: 'refreshFlag',
	refreshParam: 'refreshParam',
	commonParam: 'commonParam',
}

export const STORE_KEY_OPTS = [...Object.values(STORE_KEY_ENUM)]